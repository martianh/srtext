import os
import pytest

from io import BytesIO

from flask import Flask, request, session
from flask_session import Session

import srtext

UPLOAD_FOLDER = "uploads/"
OUTPUT_FOLDER = "output/"
MODELS_FOLDER = "models/"


# my mini app set up for testing:
@pytest.fixture
def client():
    app = srtext.create_app()
    app.config['SECRET_KEY'] = 'sekrit!'
    app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    app.config["MAX_CONTENT-LENGTH"] = 1024 * 1024 * 1024  # 1GB in bytes
    app.testing = True

    # Session: ends on browser close
    app.config["SESSION_PERMANENT"] = False
    app.config["SESSION_TYPE"] = "filesystem"
    Session(app)

    client = app.test_client()
    yield client

# testing the '/' route:
def test_root(client):
    "Test out a GET request to this pytest fixture and client"
    rv = client.get('/')
    # just testing that the following string is somewhere in the data
    assert b'Upload a Video File' in rv.data
    assert b'Password' in rv.data

# test a log in attempt:
def login(client, password):
    # POST request, form data is always a dict??:
    return client.post('/', data=dict(
        password=password
    ), follow_redirects=True)

def test_login(client):
    """Make sure login works."""
    with open(srtext.PASSFILE, "r") as pf:
        password = pf.read().replace("\n", "")

        with client as c:
            # flask tutorial just tests for flash messages like so:
            # i guess they only appear if the command runs properly.
            rv = login(client, f'{password}x')
            assert b'Invalid password, try again:' in rv.data
            assert rv.status_code == 200
            # if no login, 'status' doesn't exist:
            # assert session['status'] == ''

            rv = login(client, password)
            assert b'Logged in' in rv.data
            assert rv.status_code == 200
            assert session['status'] == 'good'

# NB: try to write UNIT tests first, simple checks
# for each element; then worry about how to do integration

# testing upload route:
def test_file_upload(client):
    "Test a POST request to /upload, when we are logged in."

    up_list = os.listdir(UPLOAD_FOLDER)
    assert len(up_list) == 0

    # dummy upload file, first name must match your name in the html form!:
    data = {'files[]' : (BytesIO(b'test-file'), 'test_file.mp4')}

    with client as c:
        with open(srtext.PASSFILE, "r") as pf:
            password = pf.read().replace("\n", "")
            login(client, password)  # works, sets session

        rv = client.post('/upload', data=data, follow_redirects=True)
        assert rv.status_code == 200
        assert b'file test_file.mp4 uploaded successfully' in rv.data

        up_list = os.listdir(UPLOAD_FOLDER)
        assert len(up_list) == 1

# testing upload route redirect:
def test_upload_redir(client):
    "Test a POST request to /upload when not logged in. It shd redirect to '/'."
    rv = client.post('/upload')
    assert rv.status_code == 302
    assert b'Redirecting' in rv.data

# testing upload route file not allowed:
def test_upload_allowed(client):
    "Test a POST request to /upload with wrong file type. It shd redirect to '/upload', and there should be no files in uploads folder."

    data = {'files[]' : (BytesIO(b'test-file'), 'test_file.pdf')}

    with client as c:
        with open(srtext.PASSFILE, "r") as pf:
            password = pf.read().replace("\n", "")
            login(client, password)  # works, sets session

        rv = client.post('/upload', data=data, follow_redirects=True)
        assert rv.status_code == 200
        assert b'Allowed file types are:' in rv.data

        up_list = os.listdir(UPLOAD_FOLDER)
        assert len(up_list) == 0

# TODO: test clearing of files on GET to /upload
def test_upload_get(client):
    "Test a GET request to /upload, when we are logged in. Files should be cleared. GET is mostly to happen when user clicks the 'start over' button."

    with client as c:
        with open(srtext.PASSFILE, "r") as pf:
            password = pf.read().replace("\n", "")
            login(client, password)  # works, sets session

        rv = client.get('/upload', follow_redirects=True)
        assert rv.status_code == 200
        assert b'Starting over' in rv.data

        up_list = os.listdir(UPLOAD_FOLDER)
        out_list = os.listdir(OUTPUT_FOLDER)
        assert len(out_list) == 0;
        assert len(up_list) == 0;


# TODO: test write_subs function how?

# TODO: test removing files on download

# TODO: test max file upload setting

# TODO: test correct model chosen
