#!/usr/bin/env python3

import os
import zipfile
import glob
from werkzeug.utils import secure_filename
from flask import (
    Flask,
    request,
    redirect,
    send_file,
    render_template,
    flash,
    session,
    after_this_request,
)
from flask_session import Session

from flask_dropzone import Dropzone

import subextract

UPLOAD_FOLDER = "uploads/"
OUTPUT_FOLDER = "output/"
MODELS_FOLDER = "models/"

# global vars
filename = ""
filename_with_path = ""
# FILES = [] # doesn't work!

ALLOWED_EXTENSIONS = set(["mp4", "avi", "mov", "wmv", "mkv", "mpg", "mpeg"])


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def write_subs(filename, output_file):
    filename_with_path = UPLOAD_FOLDER + filename
    output_file_with_path = OUTPUT_FOLDER + output_file
    model = MODELS_FOLDER + request.form["Source Language"]

    # write the subs:
    it = enumerate(subextract.gen_subparts(filename_with_path, model, True, 4, False))
    print(f"writing subtitles to {output_file_with_path}.")
    with open(output_file_with_path, "a") as outfile:  # append
        for i, subpart in it:
            n = i + 1
            outfile.write(
                f"""{n}
{subpart}

"""
            )
    print(f"file {output_file_with_path} created.")


def zipfiles():
    "Zip srt files in OUTPUT_FOLDER for download."
    # go get files with no path in our zipfile:
    os.chdir(OUTPUT_FOLDER)
    zfiles = glob.glob("*.srt")
    zipz = "srtfiles.zip"
    print(f"{zfiles} are the files to zip")

    with zipfile.ZipFile(zipz, "w") as zipMe:
        for f in zfiles:
            zipMe.write(f)
            print(f"{f} added to zip file {zipz}")
    os.chdir("..")
    return zipz


def remove_files():
    try:
        files = glob.glob(OUTPUT_FOLDER + "/*")
        print(f"trying to remove srt files {files}")
        if files != []:
            for f in files:
                os.remove(f)
                continue
            print(f"files {files} removed")
    except Exception as error:
        app.logger.error("Error removing downloaded srt file(s)", error)
    try:
        files = glob.glob(UPLOAD_FOLDER + "/*")
        print(f"trying to remove media files {files}")
        if files != []:
            for f in files:
                os.remove(f)
                continue
            print(f"files {files} removed")
    except Exception as error:
        app.logger.error("Error removing uploaded media file(s)", error)


# PASSWORD:
PASSFILE = "password"


def password_prompt(message):
    return render_template("srtext-auth.html", message=message)


# Flask app factory
def create_app():
    # app and config
    app = Flask(__name__, static_folder="static", template_folder="templates")
    app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    app.config["MAX_CONTENT-LENGTH"] = 1024 * 1024 * 1024  # 1GB in bytes
    app.secret_key = b"yMzpRPvT{G<;#kBR"

    # dropzone config:
    dropzone = Dropzone(app)

    app.config.update(
        DROPZONE_MAX_FILE_SIZE=1024,  # 1GB in MB
        DROPZONE_TIMEOUT=20 * 60 * 1000,  # 20 minutes
        DROPZONE_ALLOWED_FILE_CUSTOM=True,
        DROPZONE_SERVE_LOCAL=True,
        DROPZONE_ALLOWED_FILE_TYPE="video/*",
        # DROPZONE_REDIRECT_VIEW = 'processing',
        # DROPZONE_UPLOAD_ON_CLICK=True
        # DROPZONE_PARALLEL_UPLOADS=3,
        # DROPZONE_UPLOAD_MULTIPLE=True
    )

    # Session: ends on browser close
    app.config["SESSION_PERMANENT"] = False
    app.config["SESSION_TYPE"] = "filesystem"
    Session(app)

    # LOGIN
    @app.route("/", methods=["GET", "POST"])
    def login():
        print(request.method)
        if request.method == "GET":
            return password_prompt("Password")
        elif request.method == "POST":
            with open(PASSFILE, "r") as pf:
                password = pf.read().replace("\n", "")
                if request.form["password"] != password:
                    return password_prompt("Invalid password, try again:")
                else:
                    session["status"] = "good"
                    print("Password accepted")
                    flash("Logged in")
                    return redirect("/upload")
        #return render_template('srtext-auth.html', message=message)

    # nb, with dropzone, each single file dropped in runs a post request.

    # UPLOAD API
    @app.route("/upload", methods=["GET", "POST"])
    def upload_file():
        # auth first:
        if session.get("status") != "good":
            return redirect("/")
        else:
            if request.method == "GET":
                flash("Starting over") # displays on page reload too
                # remove everything on click 'start over' from processing
                @after_this_request
                def remove_file(response):
                    remove_files()
                    return response

            if request.method == "POST":
                # zeroing this means each dropzone upload starts with it empty:
                files = []
                # check if the post request has the file part
                # if "files[]" not in request.files:
                    # print("no file")
                    # flash("No file found. Try again.", "error")
                    # return redirect(request.url)
                # else:
                if 'X-Requested-With' in request.headers:
                    if request.headers['X-Requested-With'] == "XMLHttpRequest":
                        # dropzone files collect, use items() not .getlist("files[]"):
                        for key, f in request.files.items():
                            if key.startswith("file"):
                                files.append(f)
                                continue
                else:
                    # non-dropzone files collect:
                    files = request.files.getlist("files[]")
                file_count = len(files)
                print(file_count)
                print(f"we will process {files}")
                if len(files) == 1 and files[0].filename == "":
                    print("no filenames")
                    flash("You need to select at least one file to upload.", "error")
                    return redirect(request.url)
                # save them:
                for ufile in files:
                    try:
                        filename = secure_filename(ufile.filename)
                        filename_with_path = UPLOAD_FOLDER + filename
                        if ufile and allowed_file(filename):
                            if os.path.exists(filename_with_path):
                                print("file already uploaded, skipping")
                                flash(f"file {filename} already uploaded.")
                            else:
                                ufile.save(filename_with_path)
                                print("saved file successfully")
                                flash(f"file {filename} uploaded successfully.")
                        else:
                            flash(
                                f"Allowed file types are: {ALLOWED_EXTENSIONS}", "error"
                            )
                            return redirect(request.url)
                    except OSError as e:
                        flash(f"Error uploading file: {e}")
                return redirect("/processing")
            return render_template("srtext-upload.html", exts=ALLOWED_EXTENSIONS)

    # PROCESS
    @app.route("/processing", methods=["GET", "POST"])
    def srt_from_file():
        output_file_list_path = []  # no leftovers from prev downloads
        input_file_list = []  # clear it just in case

        for (dirs, dirnames, files) in os.walk(UPLOAD_FOLDER):
            for f in files:
                input_file_list.append(f)

        # handle hitting Next under dropzone when no files:
        if request.method == "GET":
            if input_file_list == []:
                print("no filename")
                flash(
                    "You need to select a file to upload. Files are removed after processing.",
                    "error",
                )
                return redirect("/upload")

        if request.method == "POST":
            # handle second hit of 'gen .srt' button after download:
            if input_file_list == []:
                print("no filename")
                flash(
                    "You need to select a file to upload. Files are removed after processing.",
                    "error",
                )
                return redirect("/upload")
            else:
                print(f"we will process the following files: {input_file_list}.")
                for filename in input_file_list:
                    print(f"processing {filename}")
                    filename_with_path = UPLOAD_FOLDER + filename
                    output_file = (os.path.splitext(filename)[0]) + ".srt"
                    output_file_with_path = OUTPUT_FOLDER + output_file

                    if os.path.isfile(filename_with_path):
                        write_subs(filename, output_file)

                    output_file_list_path.append(output_file_with_path)

                # if we have a list, zip it:
                if len(output_file_list_path) > 1:
                    z = zipfiles()
                    return redirect("/return-files/" + z)

                # single file no zip:
                else:
                    return redirect("/return-files/" + output_file)
        return render_template("srtext-processing.html")

    # DOWNLOAD API
    @app.route("/return-files/<filename>")
    def return_files_tut(filename):
        out_file_path = OUTPUT_FOLDER + filename

        # remove srt afterwards:
        @after_this_request
        def remove_file(response):
            remove_files()
            return response

        if os.path.isfile(out_file_path):
            return send_file(out_file_path, as_attachment=True, download_name="")
        else:
            flash("Error, no file. Files are deleted upon processing. Try again.")
            return redirect("/upload")

    # flask application:
    return app


# disable debug on prod!!!
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001)
